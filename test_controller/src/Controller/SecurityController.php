<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception as ExceptionException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    private $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/deconnexion", name="deconnexion")
     */
    public function logout()
    {
    }

    /**
     * @Route("/users", name="users")
     */
    public function users()
    {
        $users = $this->userRepository->findAll();
        return $this->render(
            'security/users.html.twig',
            array('users' => $users)
        );
    }

    /**
     * @Route("/user/{username}", name="user")
     */
    public function userById($username)
    {
        $user = $this->userRepository->findBy(array('username' => $username));
        if ($user) {
            return new Response('it\'s okkey');
        }
        else {
            return new Response("no body here");
        }
    }

    //Now notre dernier contrôleur celui de la page d'accueil interne

    /**
     * @Route("/acceuil", name="acceuil")
     */
    public function acceuil()
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('security/acceuil.html.twig');
    }
}
