<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }


    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setUsername('hazem');
        $user1->setEmail('hazem.lahmer@talan.com');
        $user1->setPassword($this->passwordEncoder->encodePassword($user1, 'hazem'));
        $user1->setRoles('ROLE_USER');
        $manager->persist($user1);
        $manager->flush();

        $user2 = new User();
        $user2->setUsername('sahar');
        $user2->setEmail('sahar.ben-khoud@talan.com');
        $user2->setPassword($this->passwordEncoder->encodePassword($user2, 'sahar'));
        $user2->setRoles('ROLE_ADMIN');
        $manager->persist($user2);
        $manager->flush();
    }
}
