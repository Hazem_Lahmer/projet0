<?php

namespace App\Tests\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PHPUnit\Framework\Constraint\ExceptionMessage;
use PHPUnit\Framework\Exception as FrameworkException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityControllerTest extends WebTestCase
{
    public function testLoginOnBadPassword()
    {
        $client = self::createClient();

        $crawler = $client->request('GET', '/login');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Sign in')->form();

        $form['_password'] = "123";

        $form['last_username'] = "hazem";

        $crawler = $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirect("http://localhost/login"));

        $crawler = $client->followRedirect();

        $this->assertTrue($client->getResponse()->isSuccessful(), 'response status is 2xx');

        $this->assertContains('Invalid credentials', $client->getResponse()->getContent());
    }

    public function testLoginOnBadUsername()
    {
        $client = self::createClient();

        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('Sign in')->form();

        $form['_password'] = "hazem";

        $form['last_username'] = "hamza";

        $crawler = $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirect("http://localhost/login"));

        $crawler = $client->followRedirect();

        $this->assertContains('Invalid credentials', $client->getResponse()->getContent());
    }

    public function testLoginOnSuccess()
    {
        $client = self::createClient();

        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('Sign in')->form();

        $form['_password'] = "hazem";

        $form['last_username'] = "hazem";

        $crawler = $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirect("http://localhost/acceuil"));

        $crawler = $client->followRedirect();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testLogout()
    {
        $client = self::createClient();

        $client->request('GET', '/deconnexion');

        $this->assertTrue($client->getResponse()->isRedirect("http://localhost/login"));

        $client->followRedirect();

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testAcceuilUser()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'hazem',
            'PHP_AUTH_PW'   => 'hazem',
        ]);

        $crawler = $client->request('GET', '/acceuil');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertSame(1, $crawler->filter('html body:contains("Bonjour Utilisateur")')->count());
    }

    public function testAcceuilAdmin()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'sahar',
            'PHP_AUTH_PW'   => 'sahar',
        ]);

        $crawler = $client->request('GET', '/acceuil');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertSame(1, $crawler->filter('html body:contains("Bonjour Administrateur")')->count());
    }

    public function testUsers()
    {
        $client = self::createClient();

        $crawler = $client->request('GET', '/users');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testUserSavingInDatabase()
    {

        $userCreated = new User();
        $userCreated->setUsername('test');
        $userCreated->setEmail('test@test.com');
        $userCreated->setPassword('0000');
        $userCreated->setConfirmedPassword('0000');
        $userCreated->setRoles('ROLE_ADMIN');


        $userRepository = $this->createMock(UserRepository::class);
        $userRepository->expects($this->any())
            ->method('findBy')
            ->willReturn($userCreated);

        $kernel = self::bootKernel();
        $em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $em->persist($userCreated);
        $em->flush();

        $user = $userRepository->findBy(array('username' => $userCreated->getUsername()));

        $this->assertInstanceOf(User::class, $user);

        $this->assertSame($user, $userCreated);

    }

    public function testUserByIdOnSuccess()
    {
        $client = self::createClient();
        $client->request('GET', '/user/hazem');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('it\'s okkey', $client->getResponse()->getContent());
    }
    
    public function testUserByIdOnFailure()
    {
        $client = self::createClient();
        $client->request('GET', '/user/inexistantName');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('no body here', $client->getResponse()->getContent());
    }
}
