*Installation: test_unitaires
--------------

>composer install

>php bin/console d:d:c 

>php bin/console  d:s:u -f

>php bin/console doctrine:fixtures:load

>php -S 127.0.0.1:8000 -t public
ou
>php bin/connsole serve:run

*Générer une clé publique et privée avec une passphrase à reporter dans le .env:
---------------------------------------------------------------------------------

$ mkdir -p config/jwt

$ openssl genrsa -out config/jwt/private.pem -aes256 4096

$ openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem

Lancer les tests
-----------------

#test coverage

>vendor/bin/codecept run --coverage --coverage-html

#Lancer les test

>vendor/bin/codecept run