<?php

namespace App\Controller\Rest;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;



/**
 * Class UeerController
 * @package App\Controller\Rest
 * @Route("/api/users", name="users")
 */
class UserController extends  AbstractFOSRestController
{
    protected $em;
    protected $passwordEncoder;
    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Retrieves a collection of User resource
     * @Rest\Get("/")
     * @Rest\View(serializerGroups={"users"})
     */
    public function getUsers() : View
    {
        $users = $this->em->getRepository(User::class)->findAll();

        // In case our GET was a success we need to return a 200 HTTP OK response with the collection of article object
        return View::create($users, Response::HTTP_OK);
    }

    /**
     * Retrieves a User resource
     * @Rest\Get("/{id}")
     * @Rest\View(serializerGroups={"users"})
     */
    public function getUsersById(int $id): View
    {
        $user = $this->em->getRepository(User::class)->findOneBy(['id' => $id]);

        // In case our GET was a success we need to return a 200 HTTP OK response with the request object
        return View::create($user, Response::HTTP_OK);
    }

    /**
     * Creates a USer resource
     * @Rest\Post("")
     * @Rest\View(serializerGroups={"users"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return View
     */
    public function postUsers(Request $request, UserPasswordEncoderInterface $passwordEncoder) : View
    {
        $user = new User();

        $params = $request->request->all();

        $user->setUsername($params['username']);
        $user->setEmail($params['email']);
        $user->setPassword($passwordEncoder->encodePassword($user,$params['password']));
        $user->setRoles('ROLE_ADMIN');

        $this->em->persist($user);
        $this->em->flush();

        // In case our POST was a success we need to return a 201 HTTP CREATED response
        return View::create($user, Response::HTTP_CREATED);
    }

    /**
     * Replaces User resource
     * @Rest\Put("/{id}")
     * @Rest\View(serializerGroups={"users"})
     * @throws \Exception
     */
    public function putUser(int $id, Request $request, UserPasswordEncoderInterface $passwordEncoder): View
    {
        $user = $this->em->getRepository(User::class)->findOneBy(['id'=>$id]);

        if($user){
            $params = $request->request->all();

            $user->setUsername($params['username']);
            $user->setEmail($params['email']);
            $user->setPassword($passwordEncoder->encodePassword($user, $params['password']));
            $user->setRoles('ROLE_USER');

            $this->em->persist($user);
            $this->em->flush();
        }
        // In case our PUT was a success we need to return a 200 HTTP OK response with the object as a result of PUT
        return View::create($user, Response::HTTP_OK);
    }

    /**
     * Removes the User resource
     * @Rest\Delete("/{id}")
     * @Rest\View(serializerGroups={"users"})
     */
    public function deleteUser(int $id): View
    {
        $user = $this->em->getRepository(User::class)->findOneBy(['id'=>$id]);
        if ($user) {
            $this->em->remove($user);
            $this->em->flush();
        }
        // In case our DELETE was a success we need to return a 204 HTTP NO CONTENT response. The object is deleted.
        return View::create([], Response::HTTP_NO_CONTENT);
    }

}
