<?php namespace App\Tests;
use App\Tests\ApiTester;

class UserCest
{
    public function _before(ApiTester $apiTester)
    {
        $apiTester->connectApi();
    }

    /**
     * @param ApiTester $apiTester
     */
    public function getUsers(ApiTester $apiTester)
    {
        $apiTester->wantTo('get all users');
        $apiTester->sendGET('api/users');
        $apiTester->seeResponseCodeIs(200);
    }

    /**
     * @param ApiTester $apiTester
     */
    public function getUsersById(ApiTester $apiTester)
    {
        $apiTester->wantTo('get an only user');
        $apiTester->sendGET('api/users/1');
        $apiTester->seeResponseCodeIs(200);
    }


    /**
     * @param \App\Tests\ApiTester $apiTester
     */
    public function addUser(\App\Tests\ApiTester $apiTester)
    {
        $apiTester->wantTo('POST user');
        $apiTester->sendPost('api/users',
            [
                "email" => "test1@talan.com",
                "username" => "test",
                "password" => "aaaa",
                "role" => "ROLE_USER",
            ]);
        $apiTester->seeResponseCodeIs(201);

    }

    /**
     * @param \App\Tests\ApiTester $apiTester
     */
    public function putUser(\App\Tests\ApiTester $apiTester)
    {
        $apiTester->wantTo('PUT user');
        $apiTester->sendPUT('api/users/1',
            [
                "email" => "test1@talan.com",
                "username" => "test1",
                "password" => "aaaaaaa",
                "role" => "ROLE_USER",
            ]);
        $apiTester->seeResponseCodeIs(200);
    }

    /**
     * @param \App\Tests\ApiTester $apiTester
     */
    public function deleteUser(\App\Tests\ApiTester $apiTester)
    {
        $apiTester->wantTo('delete user');
        $apiTester->sendDELETE('api/users/2');
        $apiTester->seeResponseCodeIs(204);
    }


}
